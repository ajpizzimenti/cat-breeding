#if __name__ == '__main__':
#    import matplotlib
#    matplotlib.use('Agg')

import numpy as np

import qubit_oscillator.feedback as fb
import qubit_oscillator.simulation.fixed_result as sim_fr
import qubit_oscillator.simulation.shared as shared
import qubit_oscillator.wigner3d as w3d
from qubit_oscillator.misc.other import alarm, memory_limit
from qubit_oscillator.settings import N

import qutip as qt


def plot_helper(i, figname, state_list):
    figname_complete = PLOT_PATH + figname + '_' + str(i) + '.png'
    w3d.plot_wigner(state_list[i], figname=figname_complete,
                        encoding_mode=measured_op_type, lim=4)

if __name__ == "__main__":
    # Set 5.5 Gb resource limit on Linux systems

    PLOT_PATH = './RESULTS/talk/'

    protocol = "VSG"
    assert protocol in ("VSG", "PostProcess", "Oscillator")

    memory_limit(5.5)

    error_type = 'noiseless'
    delta = 0.3
    target_op = np.sqrt(np.pi)                         # Targeted final spacing
    measured_op_type = 'M'

    M = 3                                              # Nr. of rounds

    #result_list = result_random(4, M)
    result_list = [[0] * M]
    exp_info_param = {'H_error_c': None,
                          'noise_ops': None,
                          'error_rate': None,
                          'mixed_projection': False,
                          'imperfect_projection': False,
                          'imperfect_rotation': False,
                          'method': 'circuit',
                          'delta': delta}
    state_list = []
    ev_list = []
    twodim=False
    if protocol == "PostProcess":
        # Breeding with our efficient protocol:
        # SCS + Beam splitters, parallel setup
        # No post-selection
        # Selects traces with large shifts to make effect visible

        #Record 2: [(-2.63671875, -4.5166015625), (5.029296875,)]
        #Phases 2: [2.3049228424411194, 15.523470384440534, -20.235637795245882, 2.4072445683642281]
        #Record 1: [[[-2.63671875]], [[-4.5166015625]]]
        #Phases 1: [[-4.673462302192279, 4.673462302192279], [-8.0054678324589954, 8.0054678324589954]]

        #Mu: 0.2385135905458471, Delta: 0.32248089801652097
        #Record 2: [(-1.26953125, 0.1220703125), (0.732421875,)]
        #Phases 2: [-1.8840589539464485, 4.4804268996088252, -0.9921990760256465, -1.6041688696367307]
        #Record 1: [[[-1.26953125]], [[0.1220703125]]]
        #Phases 1: [[-3.1822429267776373, 3.1822429267776373], [0.30598489680554203, -0.30598489680554203]]

        figname = 'efficient'

        phase_list = [
                [[0], [0], [0], [0]],
                [[2.815061050610987, -2.815061050610987], [0.18359093808332522, -0.18359093808332522]],
                [[15.147808792507277, 9.517686691285304, -12.149156803812964, -12.516338679979615]]
                ]
        for m in range(M + 1):
            state_row = []
            for phases in phase_list[m]:
                print(m)
                encoding_param = {'n_m': [[0] * 2**m],
                                  'mode': 'list',
                                  'phases': phases,
                                  'error_rate': None,
                                  'm': 2**m}
                measured_op = target_op * np.sqrt(2)**(M-m)
                exp_info = shared.Exp_Info(measured_op=measured_op, **exp_info_param)

                encoding = fb.Encoding(**encoding_param)
                state_ = sim_fr.final_state(encoding, exp_info, delta=delta,
                                               track_states=True,
                                               measured_op_type=measured_op_type
                                               )
                state_row.append(state_[-1])
            if m == M:
                state_row.append(shared.correcting_shift_ev(state_row[-1], exp_info))
            state_list.append(state_row)

    elif protocol == "VSG":
        figname = 'VSG'
        # Breeding with the VSG protocol:
        # SCS + Beam splitters, parallel setup, post-select onto p=0

        for m in range(0, M):
            # Set the spacing in the current round m
            measured_op = target_op * np.sqrt(2)**(M-m-1)
            exp_info = shared.Exp_Info(measured_op=measured_op, **exp_info_param)

            # we want 2^m applications of the measurement operator in round m
            # Set the phase to 0 (post-selection)
            encoding_param = {'n_m': [0] * 2**m,
                          'mode': 'off',
                          'error_rate': None,
                          'm': 2**m}

            encoding = fb.Encoding(**encoding_param)

            state_row = sim_fr.final_state(encoding, exp_info, delta=delta,
                                               track_states=True,
                                               measured_op_type=measured_op_type
                                               )
            state_list.append(state_row[-1])
        for i, state_row in enumerate(state_list):
            w3d.plot_contour(state_row, figname=(PLOT_PATH + figname + '_zoom_' + str(i) + '.pdf'),
                            encoding_mode=measured_op_type, lim=1.3, scale='square', labels=False,
                            cross=True
                            )
            w3d.plot_contour(state_row, figname=(PLOT_PATH + figname + str(i) + '.pdf'),
                            encoding_mode=measured_op_type, lim=3.5, scale='square', labels=False,
                            cross=True
                            )
            w3d.plot_contour(state_row, figname=(PLOT_PATH + figname+ "_label_" + str(i) + '.pdf'),
                            encoding_mode=measured_op_type, lim=3.5, scale='talk', labels=True,
                            cross=True
                            )
            w3d.plot_contour(state_row, figname=(PLOT_PATH + figname+ "_label_z_" + str(i) + '.pdf'),
                            encoding_mode=measured_op_type, lim=1.3, scale='talk', labels=True,
                            cross=True
                            )
        raise Exception

    elif protocol == "Oscillator":
        figname = 'HO'

        # Set the spacing in the current round m
        vac = qt.basis(N, 0)
        sq_vac = qt.squeeze(N, np.log(1 / delta)) * vac
        sq_vac_p = qt.displace(N, np.sqrt(np.pi)) * sq_vac
        sq_vac_q = qt.displace(N, 1j*np.sqrt(np.pi)) * sq_vac
        state_list = [vac, sq_vac, sq_vac_p, sq_vac_q]
        for i, state_row in enumerate(state_list):
            w3d.plot_contour(state_row, figname=(PLOT_PATH + figname + '_zoom_' + str(i) + '.png'),
                            encoding_mode=measured_op_type, lim=1.3, scale='auto', progress_bar=False
                            )
            w3d.plot_contour(state_row, figname=(PLOT_PATH + figname + str(i) + '.png'),
                            encoding_mode=measured_op_type, lim=3.5, scale='auto', progress_bar=False
                            )
        raise Exception


#    w3d.plot_contour(state_list, figname=(PLOT_PATH + figname + '.png'),
#                        encoding_mode=measured_op_type, lim=3.5, scale='auto'
#                        #y_labels=[bin_to_int(r) for r in result_list],
#                        #y_labels=ev_list,
#                        #y_labels=['breed', 'Breed Corr', 'Off', 'Off Corr']
#                        #title='$M: {}, mode: {}, Measured: {}$'.format(M, mode, measured_op)
#                        )
    #raise Exception

    for i, state_row in enumerate(state_list):
        if twodim:
            state_row.pop(0)
        w3d.plot_contour(state_row, figname=(PLOT_PATH + figname + '_zoom_' + str(i) + '.pdf'),
                        encoding_mode=measured_op_type, lim=1.3, scale='paper'
                        )
        w3d.plot_contour(state_row, figname=(PLOT_PATH + figname + str(i) + '.pdf'),
                        encoding_mode=measured_op_type, lim=3.5, scale='paper'
                        )
        for j, state in enumerate(state_row):
            w3d.plot_contour(state, figname=(PLOT_PATH + figname + '_zoom_' + str(i) + str(j) + '.pdf'),
                        encoding_mode=measured_op_type, lim=1.3,
                        labels=False, progress_bar=False, scale='square', cross=True
                        )
            w3d.plot_contour(state, figname=(PLOT_PATH + figname + str(i) + str(j) + '.pdf'),
                        encoding_mode=measured_op_type, lim=3.5,
                        labels=False, progress_bar=False, scale='square', cross=True
                        )
            w3d.plot_contour(state, figname=(PLOT_PATH + figname + "_label_" + str(i) + str(j) + '.pdf'),
                        encoding_mode=measured_op_type, lim=3.5,
                        labels=True, progress_bar=False, scale='talk', cross=True
                        )
            w3d.plot_contour(state, figname=(PLOT_PATH + figname + "_label_z_" + str(i) + str(j) + '.pdf'),
                        encoding_mode=measured_op_type, lim=1.3,
                        labels=True, progress_bar=False, scale='talk', cross=True
                        )
    #idx = range(len(n_m) + 1)
    #parallel.parallel_map(plot_helper, idx, task_args=(figname, state_list,),
    #                      serial=False, progress_bar=True)
    print('done')
    alarm(1, 443)
