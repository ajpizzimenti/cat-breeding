#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  7 11:18:16 2017

@author: weigand
"""

import matplotlib
#matplotlib.use('Agg')

import numpy as np
import scipy.special
import scipy.integrate
import scipy.stats
import scipy.interpolate
import matplotlib.pyplot as plt

import concurrent.futures
import time
import qubit_oscillator.misc.statistics as stat
import qubit_oscillator.misc.storage as store
from qubit_oscillator.misc.other import memory_limit

import Functions as F

#from memory_profiler import profile

if False:
    memory_limit(1.7)
    X = np.linspace(-25, 25, 2**11 + 1)
    max_processes = None
if True:
    memory_limit(7)
    X = np.linspace(-60, 60, 2**13 + 1)
    #X = np.linspace(-40, 40, 2**12 + 1)
    max_processes = None


N_POINTS = 2**11 + 1         # use 2^k+1 values for romb integration

# -----------------------------------------------
# Functions
I_0 = scipy.special.i0           # Alias for the modified Bessel function
PHASE = np.linspace(-np.pi, np.pi, N_POINTS)
PHASE_DIST = 2 * np.pi / (N_POINTS - 1)
PHASE_COS = np.cos(PHASE)
PHASE_COS_R = np.cos(PHASE).reshape(1, -1)
IDX = np.arange(len(PHASE))


def parallel_helper_breed(arguments):
    np.random.seed()
    x, delta, distance, rounds = arguments
    s_p = []
    s_q = []
    p = []
    for i in range(rounds):
        state, p_ = F.breed(x, delta, i, distance)
        p.append(p_)
        state.fourier('p')
        s_p.append(state.mean_sigma(distance)[1])
        state.fourier('q')
        s_q.append(state.mean_sigma(distance)[1])
    return [np.array(s_p), np.array(s_q), np.array(p)]


def parallel_helper_binom(arguments):
    x, delta, i, distance = arguments
    state, _ = F.breed(x, delta, i, distance, result=0)
    return state.mean_sigma(distance)[1]


def parallel_helper_mises(arguments):
    np.random.seed()
    k_init, rounds = arguments
    s = []
    w = []
    for i in range(rounds):
        std, w_ = F.mises_breed(k_init, i)
        w.append(w_)
        s.append(std)
    return [np.array(s), np.array(w)]


if __name__ == '__main__':
    load = True
    check_q = False

    assert load is True

    if load:
        data, _ = store.load('./DATA/breeding_v2')
    else:
        data = {}

    rounds = 6
    repetitions = 1000

    delta = 0.2
    distance = np.sqrt(2 * np.pi)

    start_time = time.time()

    const_sensor = 0.66428247
    const_gkp = 0.46971746

    const = const_sensor

    # find initial k
    k = np.linspace(10**(-20), 20, 1000000)
    std = F.mises_std(k)
    idx = np.argmin(np.abs(std - const))
    k_init = k[idx]
    print('matching delta', k_init, F.mises_std(k_init))

    # lower bound
    if data.get('lower') is None:
        data['lower'] = F.mises_std(k_init * 2.**(np.arange(rounds)))
    if not load:
        store.store('./DATA/breeding_v2', data)

    # Mises
    if data.get('mises') is None:
        arguments = [(k_init, rounds) for _ in range(repetitions)]
        with concurrent.futures.ProcessPoolExecutor(max_processes) as executor:
            s, w = list(zip(*list(executor.map(parallel_helper_mises, arguments))))
        data['mises'] = [np.array(s), np.array(w)]
    #mises_mean, mises_std = stat.weighted_avg_and_std(s, axis=0, weights=w)
    res = stat.two_sided_weighted_avg_and_std(data['mises'][0], axis=0, weights=data['mises'][1])
    mises_mean, mises_err_sym, mises_err = res

    print('Mises')
    print('m', mises_mean)
    print('s', mises_err_sym)
    print('s-', mises_err[0])
    print('s+', mises_err[1])
    print('min', np.min(data['mises'][0], axis=0))

    if not load:
        store.store('./DATA/breeding_v2', data)

    # Breeding
    if data.get('breeding') is None:
        s = []
        p = []

        #raise Exception('All processes have the same seed!')
        arguments = [(X, delta, distance, rounds) for i in range(repetitions)]
        with concurrent.futures.ProcessPoolExecutor(max_processes) as executor:
            s_p, s_q, w = list(zip(*list(executor.map(parallel_helper_breed, arguments))))
        data['breeding'] = [np.array(s_p), np.array(s_q), np.array(w), (rounds, repetitions, delta, distance)]

    assert not check_q, 'Stored data and q-data not compatible yet'
    res = stat.two_sided_weighted_avg_and_std(data['breeding'][0], axis=0, weights=data['breeding'][1])
    #res = stat.two_sided_weighted_avg_and_std(data['breeding'][0], axis=0, weights=data['breeding'][-1])
    breed_mean, breed_err_sym, breed_err = res
    print('Breeding')
    print(data['breeding'][0].shape)
    print(data['breeding'][1].shape)
    print('m', breed_mean)
    print('s', breed_err_sym)
    print('s+', breed_err[1])
    print('s-', breed_err[0])
    print('min', np.min(data['breeding'][0], axis=0))

    if check_q:
        res = stat.two_sided_weighted_avg_and_std(data['breeding'][1], axis=0, weights=data['breeding'][-1])
        breed_mean_q, breed_err_sym_q, breed_err_q = res
        print('Breeding q')
        print('Delta', delta)
        print('m', breed_mean_q)
        print('s', breed_err_sym_q)
        print('s+', breed_err_q[1])
        print('s-', breed_err_q[1])
        print('min', np.min(data['breeding'][1], axis=0))
    if not load:
        store.store('./DATA/breeding_v2', data)

    # Binomial
    # Note: using grid uses too much memory, use post-selection
    if data.get('binom') is None:
        arguments = [(X, delta, i, distance) for i in range(rounds)]
        with concurrent.futures.ProcessPoolExecutor(max_processes) as executor:
            data['binom'] = np.array(list(executor.map(parallel_helper_binom, arguments)))
    print('Binom')
    print(data['binom'])
    print('Lower')
    print(data['lower'])
    if not load:
        store.store('./DATA/breeding_v2', data)

    plt.rc('font',**{'size':16, 'family':'serif','serif':['Computer Modern Roman']})
    matplotlib.rcParams['text.usetex'] = True

    fig, ax = plt.subplots()
    fig.set_size_inches(2*3.405, 3.405)
    # plot breeding
    plt.plot(data['binom'], label='Post-select', zorder=0)
    plt.plot(data['lower'], label='Lower', zorder=1)
    ax.errorbar(np.arange(rounds), mises_mean, yerr=[mises_err[1], mises_err[0]], label='Mises', capsize=5, zorder=2)
    ax.errorbar(np.arange(rounds), breed_mean, yerr=[breed_err[1], breed_err[0]], label='Breeding', capsize=5, zorder=3)
    if check_q:
        ax.errorbar(np.arange(rounds), breed_mean_q, yerr=breed_err_q, label='Breeding q', capsize=5, zorder=3)

    x_lbl = ax.set_xlabel('$M$', size=16)
    y_lbl = ax.set_ylabel(r'$\Delta_p$', rotation=0, size=18, labelpad=20)

    plt.legend()
    plt.ylim((0, 1))
    fig.tight_layout(pad=0)
    
    plt.savefig('RESULTS/paper/breeding.pdf', bbox_inches='tight')
