#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  7 11:18:16 2017

@author: weigand
"""

import numpy as np
import scipy.special
import scipy.integrate
import scipy.stats
import scipy.interpolate as intp
import itertools
import matplotlib.pyplot as plt
import warnings


#from memory_profiler import profile



N_POINTS = 2**11 + 1         # use 2^k+1 values for romb integration

# -----------------------------------------------
# Functions
I_0 = scipy.special.i0           # Alias for the modified Bessel function
PHASE = np.linspace(-np.pi, np.pi, N_POINTS)
PHASE_DIST = 2 * np.pi / (N_POINTS - 1)
PHASE_COS = np.cos(PHASE)
PHASE_COS_R = np.cos(PHASE).reshape(1, -1)
IDX = np.arange(len(PHASE))


class State():
    def __init__(self, x, y, type, norm=True, checks=True, quadrature='p', combined=False, normalized=False, real=False):
        self.combined = combined
        self.x = x
        self.y = y
        self.checks=checks
        self.type = type
        self.real = real                            # Set wether the wavefunc is real
        assert self.type in ('prob', 'wavef')
        self.quadrature = quadrature
        assert self.quadrature in ('p', 'q')
        self._delta_x = self.x[1] - self.x[0]
        if norm:
            self.normalized = False
            self.normalize()
        else:
            self.normalized = normalized
        self.check()
            
    def check(self):
        if self.combined:
            pass
        elif self.checks is True:
            assert np.all(np.round(np.abs(self.y[-1]), 4) == 0), np.max(np.abs(self.y[-1]))
            assert np.all(np.round(self.y[0], 4) == 0), np.round(self.y[-1], 4)
        elif self.checks == 'warn':
            if round(self.y[-1], 4) != 0:
                warnings.warn(str(round(self.y[-1], 6)))
            if round(self.y[0], 4) != 0:
                warnings.warn(str(round(self.y[0], 6)))
        else:
            pass

    def copy(self, norm=True):
        return copy(self, norm=norm)

    def fourier(self, to):
        assert to in ('p', 'q')
        assert self.quadrature in ('p', 'q')
        if to == self.quadrature:
            pass
        else:
            self.x, self. y = self._fourier(self.x, self.y, to, self.combined)
            self._delta_x = self.x[1] - self.x[0]
            self.quadrature = to
            self.normalized = False

    def _fourier(self, p, y_p, to='q', combined=False):
        n = len(p)
        q = np.arange(-n / 2, n / 2) * 2 * np.pi / (n * (p[1] - p[0]))
        if combined:
            if to == 'q':
                y_q = np.fft.fft2(y_p, norm='ortho')
                q = np.fft.fftshift(q)
            elif to == 'p':
                y_q = np.fft.ifftshift(np.fft.ifft2(y_p, norm='ortho'))
                q = np.fft.ifftshift(q)
        else:
            if to == 'q':
                y_q = np.fft.fft(y_p, norm='ortho')
                q = np.fft.fftshift(q)
            elif to == 'p':
                y_q = np.fft.ifft(y_p, norm='ortho')
                q = np.fft.ifftshift(q)
        idx = np.argsort(q)
        q = q[idx]
        y_q = y_q[idx]
        return q, y_q

    def interpolate(self, x_new):
        """ using Spline is WAY faster (2-3 times)"""
        if self.real:
            return intp.UnivariateSpline(self.x, self.y, s=0, ext="zeros")(x_new)
        else:
            warnings.warn("Wavefunc is not real, using slow interpolation")
            shape = x_new.shape
            x_new = x_new.reshape(-1)
            y_new = intp.griddata(self.x, self.y, x_new, fill_value=0, method='linear')
            y_new = y_new.reshape(shape)
            return y_new

    def mean_sigma(self, distance):
        if self.type == 'wavef':
            state = self.copy(norm=False)
            state.P()
        elif self.type == 'prob':
            state = self
        state.normalize()
        return _mean_sigma(state.x, state.y, distance)
    
    def fidelity(self, distance, offset=0):
        assert self.type == 'wavef'
        self.normalize()
        x_ = np.copy(self.x)
        peaks = distance * np.arange(np.ceil(x_[0]/distance), np.floor(x_[-1]/distance) + 1)
        peaks += offset
        peak_idx = (np.abs(x_.reshape(-1,1) - peaks.reshape(1,-1))).argmin(axis=0)
        return np.abs(np.sum(self.y[peak_idx]))
    
    def cat_fidelity(self, distance, offset=0):
        # Cat states are a special case, they are shifted by 1/2 grid length. This can be fixed exactly, here is a quick solution
        assert self.type == 'wavef'
        self.normalize()
        x_ = np.copy(self.x)
        y_ = np.copy(self.y)
        y_ = y_ * np.exp(1j * distance * x_ /2)
        peaks = distance * np.arange(np.ceil(x_[0]/distance), np.floor(x_[-1]/distance) + 1)
        peaks += offset
        peak_idx = (np.abs(x_.reshape(-1,1) - peaks.reshape(1,-1))).argmin(axis=0)
        return np.abs(np.sum(y_[peak_idx]))


    def normalize(self):
        if self.normalized is True:
            pass
        else:
            nrm = self._norm()
            self.y /= nrm
            self.normalized = True

    def _norm(self, warn=False):
        if self.type == 'wavef':
            state = self.copy(norm=False)
            state.P()
        elif self.type == 'prob':
            state = self
        nrm = scipy.integrate.simps(state.y, state.x)
        if state.combined:
            nrm = scipy.integrate.simps(nrm, state.x)
        if warn:
            warnings.warn(str((self.type, nrm)))
        if self.type == 'wavef':
            nrm = np.sqrt(nrm)
        return nrm

    def P(self):
        if self.type == 'wavef':
            self.y = abs2(self.y)
            self.type = 'prob'
        elif self.type == 'prob':
            pass
        else:
            raise Exception

    def plot(self, lim=None, quadrature=None, label=None):
        assert not self.combined
        state = copy(self, norm=False)
        if quadrature is None:
            quadrature = state.quadrature
        state.fourier(to=quadrature)
        state.P()
        state.normalize()
        plt.plot(state.x, state.y, label=label)
        if lim is not None:
            plt.xlim(lim)

    def ptrace(self, axis=-1, norm=False):
        assert self.combined
        self.y = scipy.integrate.simps(self.y, self.x, axis=axis)
        self.combined = False
        self.normalized = False
        if norm:
            self.normalize()
            

def _mean_sigma(x, y, distance):
    '''distance is given in quadrature space'''
    integrand = 1j * distance * x
    np.exp(integrand, out=integrand)
    integrand = integrand * y
    I = scipy.integrate.simps(integrand, x)
    mean = np.angle(I) / (distance)
    I = np.abs(I)**2
    sigma = np.sqrt(2 * np.log(1 / I)) / distance
    return mean, sigma            


def abs2(a, overwrite=False):
    '''
    Fast version of abs()**2
    Tested to be faster then np.abs()**2 and a.real**2 + a.imag**2
    Overwriting speeds it up further
    '''
    if overwrite:
        np.abs(a, out=a)
        np.square(a, out=a)
    else:
        b = np.abs(a)
        return np.square(b, out=b)


def bs_meas(group, result=None, axis=1):
    state = _beam_splitter(*group)
    state, r, p = _measure(state, axis=1, quadrature='p', result=result)
    return state, r, p


def _beam_splitter(state1, state2):
    assert np.all(state1.x == state2.x)
    assert state1.quadrature == state2.quadrature
    assert state1.type == state2.type
    assert not state1.combined
    assert not state2.combined
    x = np.copy(state1.x)
    x1 = x.reshape(-1, 1)
    x2 = x.reshape(1, -1)
    # Assigning the wavefunc at points (q1+q2)/sqrt(2) to the points q1 enacts the beam splitter
    # The second dim of y can be regarded as the measurement results
    y = state1.interpolate((x1 + x2) / np.sqrt(2)) * state2.interpolate((x1 - x2) / np.sqrt(2))
    real = state1.real and state2.real
    state =  State(x, y, state1.type, quadrature=state1.quadrature,
                 combined=True, checks=state1.checks, norm=False, normalized=False, real=real)
    return state


def _measure(state, axis=0, quadrature='p', result=None):
    assert state.combined
    state.fourier(quadrature)
    state_ = state.copy(norm=False)
    state_.ptrace(axis=axis)
    state_.P()
    p = state_.y
    p /= np.sum(p)
    if result is None:
        result = np.random.choice(np.arange(len(state_.x)), p=p)
    else:
        result = np.argmin(np.abs(state.x - result))

    if axis == 0:
        y = state.y[:, result]
    elif axis == 1:
        y = state.y[result, :]
    state = State(state.x, np.copy(y), type=state.type, quadrature=quadrature,
                  checks=state.checks, norm=False, normalized=False, real=state.real)    # copy necessary to free memory
    return state, state.x[result], p[result]


def cat(x, sigma, distance, checks=True):
    '''In quadrature scale'''
    return grid(x, sigma=sigma, distance=distance, phases=[0], checks=checks)


def cat3(x, sigma, distance):
    '''In quadrature scale'''
    return grid(x, sigma=sigma, distance=distance, phases=[0,0])


def coherent(x, sigma, alpha):
    '''In quadrature scale'''
    alpha /= np.sqrt(2)
    y = 1j * np.sqrt(2) * np.real(alpha) * x
    y += -(x - np.sqrt(2) * np.imag(alpha))**2 / (2 * sigma**2)
    y += 1j * np.imag(alpha) * np.real(alpha)
    np.exp(y, out=y)
    return State(x, y, 'wavef', quadrature='p')


def copy(state, norm=True):
    x = np.copy(state.x)
    y = np.copy(state.y)
    return State(x, y, state.type,
                 quadrature=state.quadrature,
                 combined=state.combined,
                 norm=norm, checks=state.checks, normalized=state.normalized,
                 real=state.real)
    

def grid(x, sigma, distance, phases, checks=True):
    y = _prefactor(phases, distance, x) * _envelope(sigma, x)
    if np.sum(np.abs(y.imag)) < 10**(-7):
        y = y.real
        real = True
    else:
        real = False
    return State(x, y, 'wavef', quadrature='p', checks=checks, real=real)


def _envelope(sigma, p):
    """
    Gaussian envelope

    USES FOURIER TRANSFORMED SIGMA!

    6-7 times faster then scipy.stats.norm.pdf
    """
    s = -sigma**2 / 2
    p_ = np.square(p)
    p_ *= s
    np.exp(p_, out=p_)
    return p_


def _prefactor(phases, distance, p):
    # Get the complex weight for each displacement
    L = len(phases)
    if L == 0:
        weights = [1.]
    else:
        weights = []
        for i in range(L + 1):
            n_comb = np.int(scipy.special.binom(L, i))
            comb = itertools.combinations(phases, i)
            comb = itertools.chain.from_iterable(comb)
            comb = np.fromiter(comb, dtype=np.complex_, count=n_comb * i)
            comb.resize((n_comb, i))
            comb = np.sum(comb, axis=1)
            comb *= 1j
            np.exp(comb, out=comb)
            comb = np.sum(comb)
            weights.append(comb)
    weights = np.array(weights)

    # get the displacements
    #   prepare p
    p_ = np.expand_dims(p, axis=-1)
    shape = np.ones(len(p_.shape), dtype=np.int)
    shape[-1] = -1
    #   prepare array of distances
    d = np.arange(-L / 2, L / 2 + 1, dtype=np.complex_)
    d *= (1j * distance)
    #   get array of displacements
    d = p_ * d.reshape(shape)
    np.exp(d, out=d)
    #   weighed sum of displacements
    d = d * weights.reshape(shape)

    # compute the operator acting on the original state
    return np.sum(d, axis=-1)


def grouper(iterable, n, fill_check=True, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    if fill_check is True:
        assert len(iterable) % n == 0
    args = [iter(iterable)] * n
    return itertools.zip_longest(*args, fillvalue=fillvalue)



def ptrace(x, y, delta_x=None, axis=-1, norm=False):
    if delta_x is None:
        delta_x = (x[-1] - x[0]) / (len(x) - 1)
    ptr = scipy.integrate.simps(y, x, axis=axis)
    if norm:
        ptr_ = ptr * np.conjugate(ptr)
        ptr /= np.sqrt(scipy.integrate.simps(ptr_, x, axis=-1))
    return ptr


def breed(x, delta, nrounds, final_dist=2 * np.sqrt(np.pi), result=None, return_record=False, checks=True):
    record = []
    if result is not None:
        warnings.warn('result fixed to {}'.format(result))
    state_list = [cat(x, delta,  final_dist * np.sqrt(2)**nrounds, checks=checks) for _ in range(2**nrounds)]
    p = [1. for _ in state_list]
    for i in range(nrounds):
        p = np.array(list(grouper(p, 2)), dtype=np.float_)
        p = np.prod(p, axis=1)
        state_gen = grouper(state_list, 2)
        state_list, r, p_ = list(zip(*[bs_meas(group, result=result) for group in state_gen]))
        p *= p_
        record.append(r)
    assert len(state_list) == 1
    if return_record:
        return state_list[-1], record
    else:
        return state_list[-1], p[-1]


def mises_breed(k_init:float, M:int)->float,float:    
    """
    Breed 2**M von Mises distributed states with parameter k_init for M rounds

    Arguments:
        k_init {float} -- initial kappa
        M {int} -- number of rounds

    Returns:
        delta {float}, weight {float} -- effective squeezing, probability to obtain the simulated results
    """
    k = np.array([k_init] * 2**M)
    w = np.array([1] * 2**M)

    # Do breeding for M rounds
    for _ in range(M):
        # Assign the states pair-wise to the beam splitters
        k = k.reshape(2, -1)
        w = w.reshape(2, -1)
        w = np.product(w, axis=0)
        w = w / np.sum(w)    # Using in-place division causes TypeError

        p = []
        # For each beam splitter, do a breeding measurement
        for i, line in enumerate(_prob_p(*k)):
            line /= np.sum(line)
            idx = np.random.choice(IDX, p=line)
            p.append(PHASE_COS[idx])
            w[i] *= line[idx]
        p = np.asarray(p)
        k = np.sqrt(k[0]**2 + k[1]**2 + 2 * k[0] * k[1] * p)
    assert len(k) == 1          # At the end of the protocol, one state is left
    weight = w[-1]
    delta = mises_std(k[-1])    # Calculate the effective squeezing, given kappa
    return delta, weight


def _prob_p(k1, k2):
    k1 = np.asarray(k1).reshape((-1, 1))
    k2 = np.asarray(k2).reshape((-1, 1))
    k = np.sqrt(k1**2 + k2**2 + 2 * k1 * k2 * PHASE_COS_R)
    norm = 2 * np.pi * I_0(k1) * I_0(k2)
    return I_0(k) / norm


def mises_std_old(k):
    return np.sqrt(1 - scipy.special.i1(k) / scipy.special.i0(k))


def mises_std(k):
    k = np.copy(k)
    if np.any(k == 0):
        warnings.warn('k=0! Total Number: ' + str(np.sum(k == 0)))
    k[k == 0] += np.finfo(float).eps
    if np.any(k == 0):
        raise Exception('k=0! '+str(np.sum(k == 0))+ ' correction broken')
    return np.sqrt(np.log(scipy.special.i0(k)**2 / scipy.special.i1(k)**2) / np.pi)

