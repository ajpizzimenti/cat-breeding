#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  7 11:18:16 2017

@author: weigand
"""

import matplotlib
#matplotlib.use('Agg')
from cycler import cycler

import numpy as np
import scipy.special
import scipy.integrate
import scipy.stats
import scipy.interpolate
import matplotlib.pyplot as plt

import concurrent.futures
import time
import qubit_oscillator.misc.statistics as stat
import qubit_oscillator.misc.storage as store
from qubit_oscillator.misc.other import memory_limit
from qubit_oscillator.misc.parallel import parallel_map

import Functions as F
import psutil
import warnings

from tqdm import tqdm
from concurrent.futures import ProcessPoolExecutor, as_completed


def parallel_process(function, array, n_jobs=16, use_kwargs=False, front_num=1, chunksize=1):
    """
        A parallel version of the map function with a progress bar.

        Args:
            array (array-like): An array to iterate over.
            function (function): A python function to apply to the elements of array
            n_jobs (int, default=16): The number of cores to use
            use_kwargs (boolean, default=False): Whether to consider the elements of array as dictionaries of
                keyword arguments to function
            front_num (int, default=3): The number of iterations to run serially before kicking off the parallel job.
                Useful for catching bugs
        Returns:
            [function(array[0]), function(array[1]), ...]
    """
    #We run the first few iterations serially to catch bugs
    if front_num > 0:
        front = [function(**a) if use_kwargs else function(a) for a in array[:front_num]]
    #If we set n_jobs to 1, just run a list comprehension. This is useful for benchmarking and debugging.
    if n_jobs==1:
        return front + [function(**a) if use_kwargs else function(a) for a in tqdm(array[front_num:])]
    #Assemble the workers
    with ProcessPoolExecutor(max_workers=n_jobs) as pool:
        #Pass the elements of array into function
        if use_kwargs:
            futures = [pool.submit(function, **a) for a in array[front_num:]]
        else:
            futures = [pool.submit(function, a) for a in array[front_num:]]
        kwargs = {
            'total': len(futures),
            'unit': 'it',
            'unit_scale': True,
            'leave': True
        }
        #Print out the progress as tasks complete
        for f in tqdm(as_completed(futures), **kwargs):
            pass
    out = []
    #Get the results from the futures.
    for i, future in tqdm(enumerate(futures)):
        try:
            out.append(future.result())
        except Exception as e:
            out.append(e)
    return front + out


CB_color_cycle = ['#377eb8', '#ff7f00', '#4daf4a',
                  '#f781bf', '#a65628', '#984ea3',
                  '#999999', '#e41a1c', '#dede00']

CB_color_cycle = ['#377eb8', '#ff7f00', '#4daf4a',
                  '#f781bf']

CB_color_cycle = ['#a65628', '#984ea3',
                  '#999999', '#e41a1c']

CB_color_cycle2 = ['#cc6677', '#332288', '#117733',
          '#aa4499', '#999933', '#88ccee',
          '#882255', '#ddcc77', '#44aa99']

CB_color_cycle3 = ['#44aa99', '#88ccee', '#117733',
          '#ddcc77', '#aa4499', '#cc6677',
          '#332288']

default_cycler = cycler('color', CB_color_cycle3)

plt.rc('lines', linewidth=2)
#plt.rc('axes', prop_cycle=default_cycler)


def run_from_ipython():
    try:
        __IPYTHON__
        return True
    except NameError:
        return False

#from memory_profiler import profile


# -----------------------------------------------

def parallel_helper_breed(arguments):
    x, delta, distance, rounds = arguments
    np.random.seed()
    s_p = []
    s_q = []
    p = []
    f_p = []
    f_q = []
    m_p = []
    m_q = []
    max_y = []
    for i in range(rounds):
        state, p_ = F.breed(x, delta, i, distance, checks=False)
        p.append(p_)
        state.fourier('p')
        m_, s_ = state.mean_sigma(distance)
        if i == 0:
            f_p.append(state.cat_fidelity(distance, offset=m_))
        else:
            f_p.append(state.fidelity(distance, offset=m_))
        s_p.append(s_)
        m_p.append(m_)
        max_y_p = state.x[np.argmax(np.abs(state.y))]
        state.fourier('q')
        m_, s_ = state.mean_sigma(distance)
        f_q.append(state.fidelity(distance, offset=m_))
        s_q.append(s_)
        m_q.append(m_)
        max_y_q = state.x[np.argmax(np.abs(state.y))]
        max_y.append((max_y_p, max_y_q))
    return [s_p, s_q, p, f_p, f_q, m_p, m_q, max_y]




def parallel_helper_binom(arguments):
    x, delta, i, distance = arguments
    state, _ = F.breed(x, delta, i, distance, result=0)
    if i == 0:
        f = state.cat_fidelity(distance)
    else:
        f = state.fidelity(distance)
    return state.mean_sigma(distance)[1], f


def parallel_helper_mises(arguments):
    np.random.seed()
    k_init, rounds = arguments
    s = []
    w = []
    for i in range(rounds):
        std, w_ = F.mises_breed(k_init, i)
        w.append(w_)
        s.append(std)
    return [np.array(s), np.array(w)]


if __name__ == '__main__':

    if run_from_ipython():
        warnings.warn("Parallelization does currenlty not work in IPython!")

    SERIAL = False
    MAX_PROCESSES = psutil.cpu_count(logical=True) - 1            # HT apparently DOES give a speed-up

    def chunksize(iterable):
        """Automatically get the chunk size for the executor"""
        return len(iterable)//(100*MAX_PROCESSES) + 1

    #memory_limit(7)
    #X = np.linspace(-60, 60, 2**13 + 1)
    X = np.linspace(-40, 40, 2**12 + 1)
    #X = np.linspace(-25, 25, 2**11 + 1)

    LOAD = True
    SAVE = False
    TALK = True

    if SAVE is not False:
        warnings.warn("SAVE is NOT false! Continue?")
        if input("YES to continue") != 'YES':
            print("Leaving")
            raise Exception
    #assert SAVE is False

    plot = True
    FUTURES = 'custom'

    check_q = False
    log_scale = True


    if LOAD:
        data, keys = store.load('./DATA/breeding')
        print(keys)
    else:
        data = {}

    #data['mises'] = None

    rounds = 7
    #rounds = 3
    repetitions = 1000
    #repetitions = 50

    delta = 0.2
    distance = np.sqrt(2 * np.pi)

    start_time = time.time()

    const_sensor = 0.66428247
    const_gkp = 0.46971746

    const = const_sensor

    # find initial k
    k = np.linspace(10**(-20), 20, 1000000)
    std = F.mises_std(k)
    idx = np.argmin(np.abs(std - const))
    k_init = k[idx]
    print('matching delta', k_init, F.mises_std(k_init))

    # lower bound
    if data.get('lower') is None:
        data['lower'] = F.mises_std(k_init * 2.**(np.arange(rounds)))
    if SAVE:
        store.store('./DATA/breeding', data)

    # Mises
    if data.get('mises') is None:
        arguments = [(k_init, rounds) for _ in range(repetitions)]
        if FUTURES is True:
            with concurrent.futures.ProcessPoolExecutor(MAX_PROCESSES) as executor:
                s, w = list(zip(*list(executor.map(parallel_helper_mises, arguments, chunksize=chunksize(arguments)))))
        elif FUTURES == 'custom':
            s, w = list(zip(*list(parallel_process(parallel_helper_mises, arguments, n_jobs=MAX_PROCESSES))))
        else:
            s, w = list(zip(*list(parallel_map(parallel_helper_mises, arguments, serial=SERIAL, num_cpus=MAX_PROCESSES))))
        data['mises'] = [np.array(s), np.array(w)]
    #mises_mean, mises_std = stat.weighted_avg_and_std(s, axis=0, weights=w)
    res = stat.two_sided_weighted_avg_and_std(data['mises'][0], axis=0, weights=data['mises'][1])
    mises_mean, mises_err_sym, mises_err = res
    print('Mises')
    print("Total time = {}".format(time.time()-start_time))
    print('m', mises_mean)
    print('s', mises_err_sym)
    print('s-', mises_err[0])
    print('s+', mises_err[1])
    print('min', np.min(data['mises'][0], axis=0))

    if SAVE:
        store.store('./DATA/breeding', data)

    # Breeding
    if data.get('breeding') is None:
        s = []
        p = []

        #raise Exception('All processes have the same seed!')
        arguments = [(X, delta, distance, rounds) for _ in range(repetitions)]
        if FUTURES is True:
            with concurrent.futures.ProcessPoolExecutor(MAX_PROCESSES) as executor:
                s_p, s_q, w, f_p, f_q, m_p, m_q, max_y = list(zip(*list(executor.map(parallel_helper_breed, arguments, chunksize=chunksize(arguments)))))
        elif FUTURES == 'custom':
            s_p, s_q, w, f_p, f_q, m_p, m_q, max_y = list(zip(*list(parallel_process(parallel_helper_breed, arguments, n_jobs=MAX_PROCESSES))))
        else:
            s_p, s_q, w, f_p, f_q, m_p, m_q, max_y = list(zip(*list(
                                                        parallel_map(parallel_helper_breed, arguments, serial=SERIAL, num_cpus=MAX_PROCESSES)
                                                        )))
        data['breeding'] = [np.array(s_p), np.array(s_q),
                            np.array(w),
                            np.array(f_p), np.array(f_q),
                            np.array(m_p), np.array(m_q), np.array(max_y),
                            (rounds, repetitions, delta, distance)]

    res = stat.two_sided_weighted_avg_and_std(data['breeding'][0], axis=0, weights=data['breeding'][2])
    breed_mean, breed_err_sym, breed_err = res
    res = stat.two_sided_weighted_avg_and_std(data['breeding'][3], axis=0, weights=data['breeding'][2])
    breed_mean_f, breed_err_sym_f, breed_err_f = res
    res = stat.two_sided_weighted_avg_and_std(data['breeding'][5], axis=0, weights=data['breeding'][2])
    breed_mean_shift, breed_err_sym_shift, breed_err_shift = res

    print('Breeding')
    print("Total time = {}".format(time.time()-start_time))
    print('m', breed_mean)
    print('s', breed_err_sym)
    print('s+', breed_err[1])
    print('s-', breed_err[0])
    print('min', np.min(data['breeding'][0], axis=0))
    print('Fidelity p', breed_mean_f)


    if check_q:
        res = stat.two_sided_weighted_avg_and_std(data['breeding'][4], axis=0, weights=data['breeding'][2])
        breed_mean_fq, breed_err_sym_fq, breed_err_fq = res
        print('Fidelity q', breed_mean_fq)

        res = stat.two_sided_weighted_avg_and_std(data['breeding'][1], axis=0, weights=data['breeding'][2])
        breed_mean_q, breed_err_sym_q, breed_err_q = res
        print('Breeding q')
        print('Delta', delta)
        print('m', breed_mean_q)
        print('s', breed_err_sym_q)
        print('s+', breed_err_q[1])
        print('s-', breed_err_q[1])
        print('min', np.min(data['breeding'][1], axis=0))
        res = stat.two_sided_weighted_avg_and_std(data['breeding'][6], axis=0, weights=data['breeding'][2])
        breed_mean_shiftq, breed_err_sym_shiftq, breed_err_shiftq = res
    if SAVE:
        store.store('./DATA/breeding', data)


    # Binomial
    # Note: using grid uses too much memory, use post-selection
    if data.get('binom') is None:
        arguments = [(X, delta, i, distance) for i in range(rounds)]
        if FUTURES is True:
            with concurrent.futures.ProcessPoolExecutor(MAX_PROCESSES) as executor:
                data['binom'] = list(zip(*list(executor.map(parallel_helper_binom, arguments, chunksize=chunksize(arguments)))))
        elif FUTURES == 'custom':
            data['binom'] = list(zip(*list(parallel_process(parallel_helper_binom, arguments, n_jobs=MAX_PROCESSES))))
        else:
            data['binom'] = list(zip(*list(
                            parallel_map(parallel_helper_binom, arguments, serial=SERIAL, num_cpus=MAX_PROCESSES)
                            )))
    print('Binom')
    print("Total time = {}".format(time.time()-start_time))
    print(data['binom'])
    print('Lower')
    print(data['lower'])
    if SAVE:
        store.store('./DATA/breeding', data)

    if plot:
        plt.rc('font', **{'size':16, 'family':'serif', 'serif':['Computer Modern Roman']})
        matplotlib.rcParams['text.usetex'] = True

        fig, ax = plt.subplots()
        fig.set_size_inches(2*3.405, 3.405)
        # plot breeding
        (b_line, _, _) = ax.errorbar(np.arange(rounds), breed_mean,
                    yerr=[breed_err[1], breed_err[0]],
                    label='Breeding', capsize=5, zorder=5, lw=2, linestyle="-")
        if not TALK:
            (m_line, _, _) = ax.errorbar(np.arange(rounds), mises_mean,
                    yerr=[mises_err[1], mises_err[0]],
                    label='von Mises', capsize=5, zorder=2, lw=2, linestyle="--")
            (l_line,) = plt.plot(data['lower'], label='Lower', zorder=0, lw=2, linestyle=":")
        (bin_line,) = plt.plot(data['binom'][0], label='Post-select', zorder=4, lw=2, linestyle="-.")
        if check_q:
            ax.errorbar(np.arange(rounds), breed_mean_q, yerr=breed_err_q, label='Breeding q', capsize=5, zorder=1)

        x_lbl = ax.set_xlabel('$M$', size=16)
        y_lbl = ax.set_ylabel(r'$\Delta_p$', rotation=0, size=18, labelpad=20)

        if TALK:
            plt.legend([b_line, bin_line], ['Breeding', 'Post-select'])
        else:
            plt.legend([b_line, m_line, bin_line, l_line], ['Breeding', 'von Mises', 'Post-select', 'Lower'])
        
        if log_scale:
            ax.set_yscale('log')
            plt.yticks([0.01, 0.05, 0.1, 0.5, 1], [0.01, 0.05, 0.1, 0.5, 1])
            plt.ylim((0.05, 1))
        else:
            plt.ylim((0, 1))
        fig.tight_layout(pad=0)

        plt.savefig('RESULTS/talk/breeding.pdf', bbox_inches='tight')

        fig, ax = plt.subplots()
        fig.set_size_inches(2*3.405, 3.405)
        # plot breeding
        plt.plot(data['binom'][1], label='Post-select', zorder=0)
        ax.errorbar(np.arange(rounds), breed_mean_f,
                        yerr=[breed_err_f[1], breed_err_f[0]],
                        label='Breeding', capsize=5, zorder=3, lw=2, linestyle=":")
        if check_q:
            ax.errorbar(np.arange(rounds), breed_mean_fq,
                        yerr=[breed_err_fq[1], breed_err_fq[0]],
                        label='Breeding q', capsize=5, zorder=3)

        x_lbl = ax.set_xlabel('$M$', size=16)
        y_lbl = ax.set_ylabel(r'$\mathcal{F}$', rotation=0, size=18, labelpad=20)

        plt.legend()
        plt.ylim((0, 8))
        fig.tight_layout(pad=0)

        plt.savefig('RESULTS/tests/fidelity.pdf', bbox_inches='tight')

        fig, ax = plt.subplots()
        fig.set_size_inches(2*3.405, 3.405)
        # plot breeding
        ax.errorbar(np.arange(rounds), breed_mean_shift,
                        yerr=[breed_err_shift[1], breed_err_shift[0]],
                        label='Breeding', capsize=5, zorder=3, lw=2, linestyle="-")
        if check_q:
            ax.errorbar(np.arange(rounds), breed_mean_shiftq,
                        yerr=[breed_err_shiftq[1], breed_err_shiftq[0]],
                        label='Breeding q', capsize=5, zorder=2)

        x_lbl = ax.set_xlabel('$M$', size=16)
        y_lbl = ax.set_ylabel(r'$\mathcal{F}$', rotation=0, size=18, labelpad=20)

        plt.legend()
        #plt.ylim((0, 1))
        fig.tight_layout(pad=0)

        plt.savefig('RESULTS/tests/shifts.pdf', bbox_inches='tight')

    print("Total time = {}".format(time.time()-start_time))
    #input("Press any key to quit")
